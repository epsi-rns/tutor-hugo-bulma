+++
type       = "post"
title      = "Susan Wong - I Wish You Love"
date       = 2018-02-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["jazz", "70s"]
slug       = "natalie-cole-i-wish-you-love"
author     = "epsi"

excerpt    = """\
  I wish you health.
  And more than wealth.
  I wish you love.
  """
+++

I wish you bluebirds in the spring\
To give your heart a song to sing

I wish you health\
And more than wealth\
I wish you love
