+++
type       = "post"
title      = "Lisa Loeb - How"
date       = 2018-09-13T07:35:05+07:00
categories = ["lyric"]
tags       = ["pop", "2010s"]
slug       = "lisa-loeb-how"
author     = "epsi"
+++

I didn't come this far for you to make this hard for me.

$$ \lim\limits_{x \to \infty} \exp(-x) = 0 $$

Why did you come here?\
You weren't invited.\
You were on the outside\
Stay on the outside.

$$ \int_0^\infty \mathrm{e}^{-x}\,\mathrm{d}x $$

And now you want to ask me "how"?\
It's like how does your heart beat, and why do you breathe?\
How does your heart beat, and why do you breathe?
