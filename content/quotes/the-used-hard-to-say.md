+++
type       = "post"
title      = "The Used - Hard to Say"
date       = 2014-03-25T07:35:05+07:00
categories = ["lyric"]
tags       = ["rock", "2000s"]
slug       = "the-used-hard-to-say"
author     = "epsi"

+++

And its hard to say how I feel today\
For years gone by\
And I cried

{{< advert src="oto-spies-01.png" >}}

It's hard to say that I was wrong\
It's hard to say I miss you

{{< advert src="oto-spies-02.png" >}}

Its hard to say I held my tongue\
It's hard to say if only
