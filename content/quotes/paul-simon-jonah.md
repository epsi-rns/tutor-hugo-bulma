+++
type       = "post"
title      = "Paul Simon - Jonah"
date       = 2014-01-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["pop", "70s"]
slug       = "paul-simon-jonah"
author     = "epsi"
+++

No one gives their dreams away too lightly\
They hold them tightly\
Warm against cold

```haskell
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
```

I know Jonah\
He was swallowed by a song
