+++
type       = "post"
title      = "Darius Rucker - Don't Think I Don't Think About It"
date       = 2017-03-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["pop", "2000s"]
slug       = "darius-rucker-think-about-it"
author     = "epsi"

excerpt    = """\
  But don't think I don't think about it.
  Don't think I don't have regrets.
  Don't think it don't get to me.
  """
+++

Saw you in the rear view standing,\
fading from my life\
<!--more-->
But I wasn't turnin' 'round\
No not this time

But don't think I don't think about it\
Don't think I don't have regrets\
Don't think it don't get to me

When we make choices, we gotta live with them\
Heard you found a real good man\
and you married him
