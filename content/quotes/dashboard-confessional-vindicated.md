+++
type       = "post"
title      = "Dashboard Confessional - Vindicated"
date       = 2015-05-15T07:15:05+07:00
categories = ["lyric"]
tags       = ["alternative", "2000s"]
slug       = "dashboard-confessional-vindicated"
author     = "epsi"

toc        = "toc-2015-05-dashboard"

related_link_ids = [
  "15051535"  # Stolen
]

+++

Vindicated\
I am selfish I am wrong\
I am right, I swear I'm right\
Swear I knew it all along\
And I am flawed, but I am cleaing up so well\
I am seeing in me now the things you swore you saw yourself

