+++
type       = "post"
title      = "Cranberries - Dreaming My Dreams"
date       = 2015-03-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["alternative", "90s"]
slug       = "cranberries-dreaming-my-dreams"
author     = "epsi"

[opengraph]
  image    = "posts/2018/kiddo-007.jpg"
+++

All the things you said to me today\
Changed my perspective in every way\
These things count to mean so much to me\
Into my faith you and your baby

I'll be dreaming my dreams with you\
I'll be dreaming my dreams with you\
And there's no other place that I'd lay down my face\
I'll be dreaming my dreams with you

![A Thinking Kiddo][image-kiddo]

-- -- --

### Example Dotfiles

ViM RC: [bandithijo][link-dotfiles]

-- -- --

### What's next ?

Our next song would be [Dashboard Confessional - Vindicated][local-whats-next]

[//]: <> ( -- -- -- links below -- -- -- )

[image-kiddo]:      {{< baseurl >}}posts/2018/kiddo-007.jpg

[local-whats-next]: {{< baseurl >}}quotes/2015/05/15/dashboard-confessional-vindicated/
[link-dotfiles]:    {{< dotfiles >}}/terminal/vimrc/vimrc.bandithijo
