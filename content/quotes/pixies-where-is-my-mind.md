+++
type       = "post"
title      = "Emily Browning - Where is My Mind"
date       = 2018-01-15T07:35:05+07:00
categories = ["lyric"]
tags       = ["alternative", "2000s"]
slug       = "pixies-where-is-my-mind"
author     = "epsi"
+++

I was swimmin' in the Caribbean\
Animals were hiding behind the rock\
Except the little fish\
But they told me, he swears\
Tryin' to talk to me
