+++
type       = "post"
title      = "Julian Baker - Sprained Ankle"
date       = 2018-09-07T07:35:05+07:00
categories = ["lyric"]
tags       = ["indie", "2010s"]
slug       = "julian-baker-sprained-ankle"
author     = "epsi"
+++

"Isn't this weather nice? Are you okay?"\
Should I go somewhere else and hide my face?

{{< highlight haskell >}}
-- Layout Hook
commonLayout = renamed [Replace "common"]
    $ avoidStruts 
    $ gaps [(U,5), (D,5)] 
    $ spacing 10
    $ Tall 1 (5/100) (1/3)
{{< / highlight >}}

A sprinter learning to wait\
A marathon runner, my ankles are sprained
