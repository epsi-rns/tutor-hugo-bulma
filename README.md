# Hugo Bulma Test Drive

An example of Hugo site using Bulma for personal learning purpose.

This is basically a Bulma version of Hugo Tutorial.

![Hugo Bulma: Preview][hugo-bulma-preview]

-- -- --

## Links

### Hugo Article Series

Consist of more than 30 articles.

* [Hugo - Overview][hugo-overview]

* [Hugo Step by Step Repository][tutorial-hugo] (this repository)

### Bulma Article Series

Consist of 10 articles.

* [Bulma - Overview][bulma-overview]

* [Bulma Step by Step Repository][tutorial-bulma]

[hugo-overview]:     https://epsi-rns.gitlab.io/ssg/2019/04/01/hugo-overview/
[bulma-overview]:    https://epsi-rns.gitlab.io/frontend/2019/03/01/bulma-overview/

[tutorial-hugo]:  https://gitlab.com/epsi-rns/tutor-hugo-bulma/
[tutorial-bulma]: https://gitlab.com/epsi-rns/tutor-html-bulma/

### Comparison

Comparation with other static site generator

* [Jekyll Step by Step Repository][tutorial-jekyll]

* [Eleventy Step by Step Repository][tutorial-11ty]

* [Hexo Step by Step Repository][tutorial-hexo]

* [Pelican Step by Step Repository][tutorial-pelican]

### Presentation

* [Concept SSG - Presentation Slide][ssg-presentation]

* [Concept CSS - Presentation Slide][css-presentation]

[tutorial-pelican]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-jekyll]:      https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-hugo]:        https://gitlab.com/epsi-rns/tutor-hugo-bulma/
[tutorial-11ty]:        https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[tutorial-hexo]:        https://gitlab.com/epsi-rns/tutor-hexo-bulma/

[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/

[ssg-presentation]:     https://epsi-rns.gitlab.io/ssg/2019/02/17/concept-ssg/
[css-presentation]:     https://epsi-rns.gitlab.io/frontend/2019/02/15/concept-css/

-- -- --

## Chapter Step by Step

### Tutor 01

> Generate Only Pure HTML

* Setup Directory for Minimal Hugo

* General Layout: Base, List, Single

* Partials: HTML Head, Header, Footer

* Additional Layout: Post

* Basic Content

![Hugo Bulma : Tutor 01][tutor-01]

Archive: [Tutor 01][archive-01]

-- -- --

### Tutor 02

* Add Bulma CSS

* Standard Header (jquery or vue) and Footer

* Enhance All Layouts with Bulma CSS

![Hugo Bulma : Tutor 02][tutor-02]

Archive: [Tutor 02][archive-02]

-- -- --

### Tutor 03

* Add Custom SASS (Custom Design)

* Nice Header and Footer

* General Layout: Terms, Taxonomy

* Apply Two Column Responsive Layout for Most Layout

* Nice Tag Badge in Tags Page

![Hugo Bulma : Tutor 03][tutor-03]

Archive: [Tutor 03][archive-03]

-- -- --

### Tutor 04

* More Content: Lyrics and Quotes. Need this content for demo

* Additional Layout: Archives

* Article Index: By Year, List Tree (By Year and Month)

* Custom Output: YAML, TXT, JSON

![Hugo Bulma : Tutor 04][tutor-04]

> Optional Feature

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

![Hugo Bulma : Tutor 04][tutor-04-widget]

Archive: [Tutor 04][archive-04]

-- -- --

### Tutor 05

* Article Index: Apply Multi Column Responsive List

* Post: Header, Footer, Navigation

> Optional Feature

* Blog Pagination: Adjacent, Indicator, Responsive

![Hugo Bulma: Tutor 05: Indicator Pagination][tutor-05-indi]

![Hugo Bulma: Tutor 05: Responsive Pagination][tutor-05-resp]

> Finishing

* Layout: Service

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes

* Post: Syntax Highlight

* Post: Bulma Title: CSS Fix

* Meta: HTML, SEO, Opengraph, Twitter

* RSS: Filter to Show Only Post Kind

![Hugo Bulma : Tutor 05: Post][tutor-05-post]

Archive: [Tutor 05][archive-05]

-- -- --

What do you think ?
  
[hugo-bulma-preview]:   https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/hugo-bulma-preview.png
[tutor-01]:         https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/14-browser-nouse-winter.png
[tutor-02]:         https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/25-browser-post.png
[tutor-03]:         https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/35-browser-post.png
[tutor-04]:         https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/44-browser-terms-tree.png
[tutor-04-widget]:  https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/51-panel-archives.png
[tutor-05-indi]:    https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/52-04-indicator-bulma-animate.gif
[tutor-05-resp]:    https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/52-04-responsive-bulma-animate.gif
[tutor-05-post]:    https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/61-blog-post-all.png

[archive-01]:       https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/tutor-hugo-01.tar
[archive-02]:       https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/tutor-hugo-02.tar
[archive-03]:       https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/tutor-hugo-03.tar
[archive-04]:       https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/tutor-hugo-04.tar
[archive-05]:       https://gitlab.com/epsi-rns/tutor-hugo-bulma/raw/master/preview/tutor-hugo-05.tar
